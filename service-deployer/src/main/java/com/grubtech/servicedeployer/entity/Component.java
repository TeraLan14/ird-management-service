package com.grubtech.servicedeployer.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Getter
@Setter
@Document("component")
public class Component extends AbstractEntity<String> {
    private String componentName;
    private String repositoryUrl;
    private String componentTypeId;
    private String componentTypeName;

    public String getComponentName() {
        return componentName;
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }
}
