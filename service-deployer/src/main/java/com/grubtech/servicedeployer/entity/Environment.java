package com.grubtech.servicedeployer.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document("environment")
public class Environment extends AbstractEntity<String> {
    private String environmentName;
    private String description;
}
