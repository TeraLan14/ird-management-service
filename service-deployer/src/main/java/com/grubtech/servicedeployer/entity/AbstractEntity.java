package com.grubtech.servicedeployer.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import net.minidev.json.annotate.JsonIgnore;
import org.springframework.data.annotation.*;

import java.time.Instant;

@Data
@SuperBuilder
@NoArgsConstructor
public class AbstractEntity<U> {
    @Id
    private String id;
    @JsonIgnore @CreatedDate
    private Instant createdAt;
    @JsonIgnore @LastModifiedDate
    private Instant modifiedAt;
    private boolean delete;
    private boolean active;
}
