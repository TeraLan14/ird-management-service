package com.grubtech.servicedeployer.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Getter
@Setter
@Document("release")
public class Release extends AbstractEntity<String> {
    private String componentId;
    private String componentName;
    private String repositoryUrl;
    private String branch;
    private String userId;
    private String description;
    private String versionNumber;
    private boolean isReleased;

}
