package com.grubtech.servicedeployer.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;


@Data
@Document("user")
public class User extends AbstractEntity<String> {
    private String username;
    private String designation;
    private String firstName;
    private String lastName;
}
