package com.grubtech.servicedeployer.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Getter
@Setter
@Document("deployment")
public class Deployment extends AbstractEntity<String> {
    private String componentId;
    private String environmentId;
    private String componentName;
    private String environmentName;
    private String repositoryUrl;
    private String versionNumber;
    private String userId;
    private boolean isDeployed;

}
