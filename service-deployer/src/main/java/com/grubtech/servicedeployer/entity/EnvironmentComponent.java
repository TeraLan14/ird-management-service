package com.grubtech.servicedeployer.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Getter
@Setter
@Document("environment_component")
public class EnvironmentComponent extends AbstractEntity<String> {
    private String componentId;
    private String componentName;
    private String environmentId;
    private String environmentName;
}
