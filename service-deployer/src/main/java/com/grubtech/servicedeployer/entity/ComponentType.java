package com.grubtech.servicedeployer.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document("component_type")
public class ComponentType extends AbstractEntity<String> {
    private String type;
}
