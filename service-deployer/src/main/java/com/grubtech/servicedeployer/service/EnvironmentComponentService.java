package com.grubtech.servicedeployer.service;


import com.grubtech.servicedeployer.entity.Component;
import java.util.List;

public interface EnvironmentComponentService  {

    List<Component> findComponentsNotInEnvironment(String environmentId);
}
