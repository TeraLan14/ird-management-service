package com.grubtech.servicedeployer.service;

import com.grubtech.servicedeployer.entity.Release;
import com.grubtech.servicedeployer.response.CircleCiResponse;
import com.grubtech.servicedeployer.response.ReleaseDeleteResponse;
import org.springframework.http.ResponseEntity;

public interface ReleaseService {

    ReleaseDeleteResponse deleteById(String releaseId);

    Release latestRelease(String componentId);

    ResponseEntity<CircleCiResponse> releaseInCircleCi(String projectSlug);
}
