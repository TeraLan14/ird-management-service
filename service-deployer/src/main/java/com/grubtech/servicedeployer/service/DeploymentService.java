package com.grubtech.servicedeployer.service;

import com.grubtech.servicedeployer.models.DeploymentEnhancedModel;
import com.grubtech.servicedeployer.models.EnvironmentDeploymentModel;
import com.grubtech.servicedeployer.response.CircleCiResponse;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface DeploymentService {

    List<DeploymentEnhancedModel> findDeploymentsForEnvironmentId(String environmentId);

    List<EnvironmentDeploymentModel> getEnvironmentsWithDeployments();

    ResponseEntity<CircleCiResponse> deployToCircleCi(String projectSlug);
}
