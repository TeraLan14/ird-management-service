package com.grubtech.servicedeployer.service;

import com.grubtech.servicedeployer.entity.Component;
import com.grubtech.servicedeployer.entity.EnvironmentComponent;
import com.grubtech.servicedeployer.repository.ComponentRepository;
import com.grubtech.servicedeployer.repository.EnvironmentComponentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EnvironmentComponentServiceImpl implements EnvironmentComponentService {

    private static final Logger logger = LoggerFactory.getLogger(EnvironmentComponentServiceImpl.class);

    @Autowired
    private ComponentRepository componentRepository;

    @Autowired
    private EnvironmentComponentRepository environmentComponentRepository;


    @Override
    public List<Component> findComponentsNotInEnvironment(String environmentId) {
        List<Component> finalComponentList = new ArrayList<>();
        List<Component> componentList = componentRepository.findAll();
        try {
            for (Component component : componentList) {
                if (environmentComponentRepository.findByEnvironmentIdAndComponentId(environmentId, component.getId()) == null) {
                    finalComponentList.add(component);
                }
            }
        }catch (Exception ex){
            logger.error(ex.getMessage());
        }
        return finalComponentList;
    }
}
