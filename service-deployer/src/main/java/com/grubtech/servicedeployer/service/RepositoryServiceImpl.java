package com.grubtech.servicedeployer.service;

import com.grubtech.servicedeployer.models.BitbucketRepositoryModel;
import com.grubtech.servicedeployer.response.repo_response.Links;
import com.grubtech.servicedeployer.response.repo_response.RepositoryResponse;
import com.grubtech.servicedeployer.response.repo_response.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import static java.util.Objects.requireNonNull;

@Service
public class RepositoryServiceImpl implements RepositoryService {

    @org.springframework.beans.factory.annotation.Value("${bitbucket.key}")
    private String key;

    @org.springframework.beans.factory.annotation.Value("${bitbucket.secret}")
    private String secret;

    @org.springframework.beans.factory.annotation.Value("${bitbucket.access_token.url}")
    private String accessTokenUrl;

    @org.springframework.beans.factory.annotation.Value("${bitbucket.repositories_api.url}")
    private String repositoriesApiUrl;

    @Override
    public List<BitbucketRepositoryModel> getAllRepositories() {

        List<BitbucketRepositoryModel> list = new ArrayList<>();
        try {

            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders header = new HttpHeaders();

            String accessToken = accessToken();
            header.setContentType(MediaType.APPLICATION_JSON);
            header.set(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken);

            HttpEntity<RepositoryResponse> requestEntity = new HttpEntity<>(header);
            ResponseEntity<RepositoryResponse> result = restTemplate.exchange(repositoriesApiUrl, HttpMethod.GET, requestEntity, RepositoryResponse.class);

            if(result.getStatusCode() == HttpStatus.OK){
                RepositoryResponse body = result.getBody();
                List<Value> valueList = body.getValues();
                for(Value value : valueList){
                    BitbucketRepositoryModel bitbucketRepositoryModel = new BitbucketRepositoryModel();

                    String slug = value.getSlug();
                    Links links = value.getLinks();
                    String href = links.getClone().get(0).getHref();

                    bitbucketRepositoryModel.setSlug(slug);
                    bitbucketRepositoryModel.setUrl(href);

                    list.add(bitbucketRepositoryModel);

                }
            }else{
                list =null;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return list;
    }

    private String accessToken(){

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("grant_type", "client_credentials");

        HttpHeaders header = new HttpHeaders();
        header.setBasicAuth(key, secret);
        header.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, header);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Object> responseEntity = restTemplate.postForEntity(accessTokenUrl, request, Object.class);

        LinkedHashMap body = (LinkedHashMap) responseEntity.getBody();
        return requireNonNull(body).get("access_token").toString();

    }
}
