package com.grubtech.servicedeployer.service;

import com.grubtech.servicedeployer.models.BitbucketRepositoryModel;

import java.util.List;

public interface RepositoryService {

    List<BitbucketRepositoryModel> getAllRepositories();
}
