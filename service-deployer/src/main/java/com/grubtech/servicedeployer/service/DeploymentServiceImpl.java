package com.grubtech.servicedeployer.service;

import com.grubtech.servicedeployer.entity.Deployment;
import com.grubtech.servicedeployer.entity.Environment;
import com.grubtech.servicedeployer.entity.Release;
import com.grubtech.servicedeployer.models.DeploymentEnhancedModel;
import com.grubtech.servicedeployer.models.EnvironmentDeploymentModel;
import com.grubtech.servicedeployer.repository.DeploymentRepository;
import com.grubtech.servicedeployer.repository.EnvironmentRepository;
import com.grubtech.servicedeployer.repository.ReleaseRepository;
import com.grubtech.servicedeployer.request.CircleciDeploymentRequest;
import com.grubtech.servicedeployer.request.Parameters;
import com.grubtech.servicedeployer.response.CircleCiResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Service
public class DeploymentServiceImpl implements DeploymentService {

    private static final Logger logger = LoggerFactory.getLogger(DeploymentServiceImpl.class);

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @org.springframework.beans.factory.annotation.Value("${bitbucket.workspace}")
    private String workspace;

    @org.springframework.beans.factory.annotation.Value("${circle-ci.url}")
    private String circleCiUrl;

    @org.springframework.beans.factory.annotation.Value("${circle-ci.token}")
    private String circleCiToken;

    @Autowired
    private DeploymentRepository deploymentRepository;

    @Autowired
    private ReleaseRepository releaseRepository;

    @Autowired
    private EnvironmentRepository environmentRepository;

    @Override
    public List<DeploymentEnhancedModel> findDeploymentsForEnvironmentId(String environmentId) {
        List<DeploymentEnhancedModel> list = new ArrayList<>();

        try {
            List<Deployment> deploymentList = deploymentRepository.findByEnvironmentId(environmentId);
            for (Deployment deployment : deploymentList) {

                DeploymentEnhancedModel model = new DeploymentEnhancedModel();
                model.setId(deployment.getId());
                model.setComponentId(deployment.getComponentId());
                model.setEnvironmentId(deployment.getEnvironmentId());
                model.setComponentName(deployment.getComponentName());
                model.setEnvironmentName(deployment.getEnvironmentName());
                model.setRepositoryUrl(deployment.getRepositoryUrl());
                model.setDeployedVersionNumber(deployment.getVersionNumber());

                List<Release> releaseList = releaseRepository.findByComponentIdOrderByCreatedAtDesc(deployment.getComponentId());
                model.setLatestVersionNumber(releaseList.get(0).getVersionNumber());

                model.setUserId(deployment.getUserId());
                model.setActive(deployment.isActive());
                model.setDeployed(deployment.isDeployed());
                model.setDelete(deployment.isDelete());

                Instant createdAt = deployment.getCreatedAt();
                Date date = Date.from(createdAt);
                model.setCreatedAt(simpleDateFormat.format(date));
                list.add(model);

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return list;
    }

    @Override
    public List<EnvironmentDeploymentModel> getEnvironmentsWithDeployments() {
        List<EnvironmentDeploymentModel> list = new ArrayList<>();
        List<Environment> environmentList = environmentRepository.findAll();
        if (environmentList != null && !environmentList.isEmpty()) {
            for (Environment environment : environmentList) {

                List<DeploymentEnhancedModel> modelList = new ArrayList<>();

                try {
                    List<Deployment> deploymentList = deploymentRepository.findByEnvironmentId(environment.getId());

                    if (deploymentList != null && !deploymentList.isEmpty()) {

                        EnvironmentDeploymentModel environmentDeploymentModel = new EnvironmentDeploymentModel();
                        environmentDeploymentModel.setId(environment.getId());
                        environmentDeploymentModel.setEnvironmentName(environment.getEnvironmentName());

                        for (Deployment deployment : deploymentList) {
                            DeploymentEnhancedModel model = new DeploymentEnhancedModel();

                            model.setId(deployment.getId());
                            model.setComponentId(deployment.getComponentId());
                            model.setEnvironmentId(deployment.getEnvironmentId());
                            model.setComponentName(deployment.getComponentName());
                            model.setEnvironmentName(deployment.getEnvironmentName());
                            model.setRepositoryUrl(deployment.getRepositoryUrl());
                            model.setDeployedVersionNumber(deployment.getVersionNumber());

                            List<Release> releaseList = releaseRepository.findByComponentIdOrderByCreatedAtDesc(deployment.getComponentId());
                            model.setLatestVersionNumber(releaseList.get(0).getVersionNumber());

                            model.setUserId(deployment.getUserId());
                            model.setActive(deployment.isActive());
                            model.setDeployed(deployment.isDeployed());
                            model.setDelete(deployment.isDelete());

                            Instant createdAt = deployment.getCreatedAt();
                            Date date = Date.from(createdAt);
                            model.setCreatedAt(simpleDateFormat.format(date));

                            modelList.add(model);

                        }

                        environmentDeploymentModel.setDeploymentEnhancedModels(modelList);
                        list.add(environmentDeploymentModel);
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        return list;
    }

    @Override
    public ResponseEntity<CircleCiResponse> deployToCircleCi(String projectSlug) {
        CircleciDeploymentRequest circleciDeploymentRequest = new CircleciDeploymentRequest();
        circleciDeploymentRequest.setBranch("master");
        circleciDeploymentRequest.setParameters(Collections.emptyMap());

        HttpHeaders httpHeaders = getHttpHeaders();
        String url = circleCiUrl + "/" + workspace + "/" + projectSlug + "/pipeline";

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<CircleCiResponse> responseEntity = null;
        try {
            responseEntity = restTemplate.postForEntity(url, new HttpEntity<>(circleciDeploymentRequest, httpHeaders), CircleCiResponse.class);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return responseEntity;
    }

    private HttpHeaders getHttpHeaders() {
        final HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBasicAuth(circleCiToken, "");
        return headers;
    }
}
