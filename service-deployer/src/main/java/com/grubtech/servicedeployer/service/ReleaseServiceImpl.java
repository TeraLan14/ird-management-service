package com.grubtech.servicedeployer.service;

import com.grubtech.servicedeployer.entity.Release;
import com.grubtech.servicedeployer.repository.ReleaseRepository;
import com.grubtech.servicedeployer.request.CircleCiReleaseRequest;
import com.grubtech.servicedeployer.request.ReleaseParameters;
import com.grubtech.servicedeployer.response.CircleCiResponse;
import com.grubtech.servicedeployer.response.ReleaseDeleteResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class ReleaseServiceImpl implements ReleaseService {

    @Autowired
    private ReleaseRepository releaseRepository;

    @org.springframework.beans.factory.annotation.Value("${bitbucket.workspace}")
    private String workspace;

    @org.springframework.beans.factory.annotation.Value("${circle-ci.url}")
    private String circleCiUrl;

    @org.springframework.beans.factory.annotation.Value("${circle-ci.token}")
    private String circleCiToken;

    @Override
    public ReleaseDeleteResponse deleteById(String releaseId) {
        ReleaseDeleteResponse releaseDeleteResponse = new ReleaseDeleteResponse();
        String result = "";
        if (releaseId != null && !releaseId.isEmpty() && !releaseId.equals("")) {
            releaseRepository.deleteById(releaseId);
            Optional<Release> release = releaseRepository.findById(releaseId);
            if (release.isPresent()) {
                result = "Cannot delete the release";
                releaseDeleteResponse.setMessage(result);
                releaseDeleteResponse.setStatusCode(HttpStatus.EXPECTATION_FAILED.value());
            } else {
                result = "The release is removed";
                releaseDeleteResponse.setMessage(result);
                releaseDeleteResponse.setStatusCode(HttpStatus.OK.value());
            }
        } else {
            result = "Cannot find the release";
            releaseDeleteResponse.setMessage(result);
            releaseDeleteResponse.setStatusCode(HttpStatus.CONFLICT.value());
        }
        return releaseDeleteResponse;
    }

    @Override
    public Release latestRelease(String componentId) {
        Release release = null;
        try {
            List<Release> releaseList = releaseRepository.findByComponentIdOrderByCreatedAtDesc(componentId);
            if (releaseList != null) {
                release = releaseList.get(0);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return release;
    }

    @Override
    public ResponseEntity<CircleCiResponse> releaseInCircleCi(String projectSlug) {
        CircleCiReleaseRequest circleCiReleaseRequest = new CircleCiReleaseRequest();
        circleCiReleaseRequest.setBranch("master");
        circleCiReleaseRequest.setParameters(Collections.emptyMap());

        HttpHeaders httpHeaders = getHttpHeaders();
        String url = circleCiUrl + "/" + workspace + "/" + projectSlug + "/pipeline";

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<CircleCiResponse> responseEntity = null;
        try{
            responseEntity = restTemplate.postForEntity(url, new HttpEntity<>(circleCiReleaseRequest, httpHeaders), CircleCiResponse.class);
        }catch (Exception ex){
            ex.printStackTrace();
        }

        return responseEntity;
    }

    private HttpHeaders getHttpHeaders() {
        final HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBasicAuth(circleCiToken,"");
        return headers;
    }
}
