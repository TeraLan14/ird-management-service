package com.grubtech.servicedeployer.controller;

import com.grubtech.servicedeployer.models.DeploymentEnhancedModel;
import com.grubtech.servicedeployer.models.DeploymentUpdateModel;
import com.grubtech.servicedeployer.models.EnvironmentDeploymentModel;
import com.grubtech.servicedeployer.response.CircleCiResponse;
import com.grubtech.servicedeployer.service.DeploymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/deployment")
public class DeploymentController {

    @Autowired
    private DeploymentService deploymentService;

    @GetMapping("/findDeploymentsForEnvironmentId/{environmentId}")
    public List<DeploymentEnhancedModel> findDeploymentsForEnvironmentId(@PathVariable("environmentId")String environmentId){
        return deploymentService.findDeploymentsForEnvironmentId(environmentId);
    }

    @GetMapping("/getEnvironmentsWithDeployments")
    public List<EnvironmentDeploymentModel> getEnvironmentsWithDeployments(){
        return deploymentService.getEnvironmentsWithDeployments();
    }

    @PostMapping(value = "/deployToCircleCi")
    public CircleCiResponse deployToCircleCi(@RequestBody DeploymentUpdateModel deploymentUpdateModel){
        ResponseEntity<CircleCiResponse> responseEntity = deploymentService.deployToCircleCi(deploymentUpdateModel.getProjectSlug());
        CircleCiResponse body = responseEntity.getBody();
        return body;
    }

}
