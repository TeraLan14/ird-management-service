package com.grubtech.servicedeployer.controller;

import com.grubtech.servicedeployer.entity.Component;
import com.grubtech.servicedeployer.service.EnvironmentComponentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/environment_component")
public class EnvironmentComponentController {

    @Autowired
    private EnvironmentComponentService environmentComponentService;

    @GetMapping("/findComponentsNotInEnvironment")
    public List<Component> findComponentsNotInEnvironment(@Param("environmentId") String environmentId) {
        return environmentComponentService.findComponentsNotInEnvironment(environmentId);
    }

}
