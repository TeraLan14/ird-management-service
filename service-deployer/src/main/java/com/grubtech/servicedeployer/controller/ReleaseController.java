package com.grubtech.servicedeployer.controller;

import com.grubtech.servicedeployer.entity.Release;
import com.grubtech.servicedeployer.models.ReleaseUpdateModel;
import com.grubtech.servicedeployer.response.CircleCiResponse;
import com.grubtech.servicedeployer.response.ReleaseDeleteResponse;
import com.grubtech.servicedeployer.service.ReleaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/release")
public class ReleaseController {

    @Autowired
    private ReleaseService releaseService;

    @DeleteMapping(value = "/deleteById")
    public ReleaseDeleteResponse deleteById(@RequestParam String releaseId) {
        return releaseService.deleteById(releaseId);
    }

    @GetMapping(value = "/latestRelease")
    public Release latestRelease(@Param("componentId") String componentId ){
        return releaseService.latestRelease(componentId);
    }

    @PostMapping(value = "/releaseInCircleCi")
    public CircleCiResponse releaseInCircleCi(@RequestBody ReleaseUpdateModel releaseUpdateModel){
        ResponseEntity<CircleCiResponse> responseEntity = releaseService.releaseInCircleCi(releaseUpdateModel.getProjectSlug());
        return responseEntity.getBody();
    }

}
