package com.grubtech.servicedeployer.controller;

import com.grubtech.servicedeployer.models.BitbucketRepositoryModel;
import com.grubtech.servicedeployer.service.RepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/bitBucket/repository")
public class BitBucketRepositoryController {

    @Autowired
    private RepositoryService repositoryService;

    @GetMapping("/getAllRepositories")
    public List<BitbucketRepositoryModel> getAllRepositories(){
        return repositoryService.getAllRepositories();
    }

}
