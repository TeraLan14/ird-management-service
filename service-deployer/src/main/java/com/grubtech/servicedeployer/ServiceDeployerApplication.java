package com.grubtech.servicedeployer;

import com.grubtech.servicedeployer.entity.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;

@SpringBootApplication
public class ServiceDeployerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ServiceDeployerApplication.class, args);
    }

    @Bean
    public RepositoryRestConfigurer repositoryRestConfigurer() {
        return new RepositoryRestConfigurer() {
            @Override
            public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
                config.exposeIdsFor(ComponentType.class, Component.class, Environment.class, Deployment.class, Release.class);
            }
        };
    }

}
