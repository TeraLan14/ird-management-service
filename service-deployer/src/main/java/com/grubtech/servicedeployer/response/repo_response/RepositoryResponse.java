
package com.grubtech.servicedeployer.response.repo_response;

import java.io.Serializable;
import java.util.List;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class RepositoryResponse implements Serializable
{

    private Long pagelen;
    private List<Value> values = null;
    private Long page;
    private Long size;
    private final static long serialVersionUID = 6202466948974654591L;

    public Long getPagelen() {
        return pagelen;
    }

    public void setPagelen(Long pagelen) {
        this.pagelen = pagelen;
    }

    public List<Value> getValues() {
        return values;
    }

    public void setValues(List<Value> values) {
        this.values = values;
    }

    public Long getPage() {
        return page;
    }

    public void setPage(Long page) {
        this.page = page;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("pagelen", pagelen).append("values", values).append("page", page).append("size", size).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(page).append(size).append(pagelen).append(values).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof RepositoryResponse) == false) {
            return false;
        }
        RepositoryResponse rhs = ((RepositoryResponse) other);
        return new EqualsBuilder().append(page, rhs.page).append(size, rhs.size).append(pagelen, rhs.pagelen).append(values, rhs.values).isEquals();
    }

}
