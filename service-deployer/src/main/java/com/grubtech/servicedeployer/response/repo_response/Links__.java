
package com.grubtech.servicedeployer.response.repo_response;

import java.io.Serializable;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Links__ implements Serializable
{

    private Self__ self;
    private Html__ html;
    private Avatar__ avatar;
    private final static long serialVersionUID = 3369110053786932734L;

    public Self__ getSelf() {
        return self;
    }

    public void setSelf(Self__ self) {
        this.self = self;
    }

    public Html__ getHtml() {
        return html;
    }

    public void setHtml(Html__ html) {
        this.html = html;
    }

    public Avatar__ getAvatar() {
        return avatar;
    }

    public void setAvatar(Avatar__ avatar) {
        this.avatar = avatar;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("self", self).append("html", html).append("avatar", avatar).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(self).append(html).append(avatar).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Links__) == false) {
            return false;
        }
        Links__ rhs = ((Links__) other);
        return new EqualsBuilder().append(self, rhs.self).append(html, rhs.html).append(avatar, rhs.avatar).isEquals();
    }

}
