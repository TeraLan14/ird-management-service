
package com.grubtech.servicedeployer.response.repo_response;

import java.io.Serializable;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Mainbranch implements Serializable
{

    private String type;
    private String name;
    private final static long serialVersionUID = 5269700371901676665L;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("type", type).append("name", name).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(type).append(name).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Mainbranch) == false) {
            return false;
        }
        Mainbranch rhs = ((Mainbranch) other);
        return new EqualsBuilder().append(type, rhs.type).append(name, rhs.name).isEquals();
    }

}
