
package com.grubtech.servicedeployer.response.repo_response;

import java.io.Serializable;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Self_ implements Serializable
{

    private String href;
    private final static long serialVersionUID = 3828878176150352548L;

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("href", href).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(href).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Self_) == false) {
            return false;
        }
        Self_ rhs = ((Self_) other);
        return new EqualsBuilder().append(href, rhs.href).isEquals();
    }

}
