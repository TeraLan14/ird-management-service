
package com.grubtech.servicedeployer.response.repo_response;

import java.io.Serializable;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Owner implements Serializable
{

    private String display_name;
    private String uuid;
    private Links___ links;
    private String type;
    private String nickname;
    private String account_id;
    private final static long serialVersionUID = -4294290818102958125L;

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Links___ getLinks() {
        return links;
    }

    public void setLinks(Links___ links) {
        this.links = links;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("display_name", display_name).append("uuid", uuid).append("links", links).append("type", type).append("nickname", nickname).append("account_id", account_id).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(account_id).append(nickname).append(links).append(display_name).append(type).append(uuid).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Owner) == false) {
            return false;
        }
        Owner rhs = ((Owner) other);
        return new EqualsBuilder().append(account_id, rhs.account_id).append(nickname, rhs.nickname).append(links, rhs.links).append(display_name, rhs.display_name).append(type, rhs.type).append(uuid, rhs.uuid).isEquals();
    }

}
