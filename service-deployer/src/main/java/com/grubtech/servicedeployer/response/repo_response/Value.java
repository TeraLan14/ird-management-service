
package com.grubtech.servicedeployer.response.repo_response;

import java.io.Serializable;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Value implements Serializable
{

    private String scm;
    private Object website;
    private Boolean has_wiki;
    private String uuid;
    private Links links;
    private String description;
    private String fork_policy;
    private String name;
    private Project project;
    private String language;
    private String created_on;
    private Workspace workspace;
    private Mainbranch mainbranch;
    private String full_name;
    private String updated_on;
    private Owner owner;
    private Boolean has_issues;
    private String type;
    private String slug;
    private Boolean is_private;
    private Long size;
    private final static long serialVersionUID = -4841968892058836707L;

    public String getScm() {
        return scm;
    }

    public void setScm(String scm) {
        this.scm = scm;
    }

    public Object getWebsite() {
        return website;
    }

    public void setWebsite(Object website) {
        this.website = website;
    }

    public Boolean getHas_wiki() {
        return has_wiki;
    }

    public void setHas_wiki(Boolean has_wiki) {
        this.has_wiki = has_wiki;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Links getLinks() {
        return links;
    }

    public void setLinks(Links links) {
        this.links = links;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFork_policy() {
        return fork_policy;
    }

    public void setFork_policy(String fork_policy) {
        this.fork_policy = fork_policy;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCreated_on() {
        return created_on;
    }

    public void setCreated_on(String created_on) {
        this.created_on = created_on;
    }

    public Workspace getWorkspace() {
        return workspace;
    }

    public void setWorkspace(Workspace workspace) {
        this.workspace = workspace;
    }

    public Mainbranch getMainbranch() {
        return mainbranch;
    }

    public void setMainbranch(Mainbranch mainbranch) {
        this.mainbranch = mainbranch;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getUpdated_on() {
        return updated_on;
    }

    public void setUpdated_on(String updated_on) {
        this.updated_on = updated_on;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public Boolean getHas_issues() {
        return has_issues;
    }

    public void setHas_issues(Boolean has_issues) {
        this.has_issues = has_issues;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public Boolean getIs_private() {
        return is_private;
    }

    public void setIs_private(Boolean is_private) {
        this.is_private = is_private;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("scm", scm).append("website", website).append("has_wiki", has_wiki).append("uuid", uuid).append("links", links).append("description", description).append("fork_policy", fork_policy).append("name", name).append("project", project).append("language", language).append("created_on", created_on).append("workspace", workspace).append("mainbranch", mainbranch).append("full_name", full_name).append("updated_on", updated_on).append("owner", owner).append("has_issues", has_issues).append("type", type).append("slug", slug).append("is_private", is_private).append("size", size).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(updated_on).append(owner).append(is_private).append(website).append(workspace).append(fork_policy).append(description).append(project).append(language).append(type).append(uuid).append(has_issues).append(mainbranch).append(has_wiki).append(full_name).append(size).append(created_on).append(name).append(links).append(scm).append(slug).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Value) == false) {
            return false;
        }
        Value rhs = ((Value) other);
        return new EqualsBuilder().append(updated_on, rhs.updated_on).append(owner, rhs.owner).append(is_private, rhs.is_private).append(website, rhs.website).append(workspace, rhs.workspace).append(fork_policy, rhs.fork_policy).append(description, rhs.description).append(project, rhs.project).append(language, rhs.language).append(type, rhs.type).append(uuid, rhs.uuid).append(has_issues, rhs.has_issues).append(mainbranch, rhs.mainbranch).append(has_wiki, rhs.has_wiki).append(full_name, rhs.full_name).append(size, rhs.size).append(created_on, rhs.created_on).append(name, rhs.name).append(links, rhs.links).append(scm, rhs.scm).append(slug, rhs.slug).isEquals();
    }

}
