
package com.grubtech.servicedeployer.response.repo_response;

import java.io.Serializable;
import java.util.List;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Links implements Serializable
{

    private List<Clone> clone = null;
    private Watchers watchers;
    private Branches branches;
    private Tags tags;
    private Commits commits;
    private Downloads downloads;
    private Source source;
    private Html html;
    private Avatar avatar;
    private Hooks hooks;
    private Forks forks;
    private Self self;
    private Pullrequests pullrequests;
    private final static long serialVersionUID = -2649309712460839490L;

    public List<Clone> getClone() {
        return clone;
    }

    public void setClone(List<Clone> clone) {
        this.clone = clone;
    }

    public Watchers getWatchers() {
        return watchers;
    }

    public void setWatchers(Watchers watchers) {
        this.watchers = watchers;
    }

    public Branches getBranches() {
        return branches;
    }

    public void setBranches(Branches branches) {
        this.branches = branches;
    }

    public Tags getTags() {
        return tags;
    }

    public void setTags(Tags tags) {
        this.tags = tags;
    }

    public Commits getCommits() {
        return commits;
    }

    public void setCommits(Commits commits) {
        this.commits = commits;
    }

    public Downloads getDownloads() {
        return downloads;
    }

    public void setDownloads(Downloads downloads) {
        this.downloads = downloads;
    }

    public Source getSource() {
        return source;
    }

    public void setSource(Source source) {
        this.source = source;
    }

    public Html getHtml() {
        return html;
    }

    public void setHtml(Html html) {
        this.html = html;
    }

    public Avatar getAvatar() {
        return avatar;
    }

    public void setAvatar(Avatar avatar) {
        this.avatar = avatar;
    }

    public Hooks getHooks() {
        return hooks;
    }

    public void setHooks(Hooks hooks) {
        this.hooks = hooks;
    }

    public Forks getForks() {
        return forks;
    }

    public void setForks(Forks forks) {
        this.forks = forks;
    }

    public Self getSelf() {
        return self;
    }

    public void setSelf(Self self) {
        this.self = self;
    }

    public Pullrequests getPullrequests() {
        return pullrequests;
    }

    public void setPullrequests(Pullrequests pullrequests) {
        this.pullrequests = pullrequests;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("clone", clone).append("watchers", watchers).append("branches", branches).append("tags", tags).append("commits", commits).append("downloads", downloads).append("source", source).append("html", html).append("avatar", avatar).append("hooks", hooks).append("forks", forks).append("self", self).append("pullrequests", pullrequests).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(forks).append(watchers).append(source).append(avatar).append(branches).append(pullrequests).append(tags).append(downloads).append(clone).append(commits).append(self).append(html).append(hooks).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Links) == false) {
            return false;
        }
        Links rhs = ((Links) other);
        return new EqualsBuilder().append(forks, rhs.forks).append(watchers, rhs.watchers).append(source, rhs.source).append(avatar, rhs.avatar).append(branches, rhs.branches).append(pullrequests, rhs.pullrequests).append(tags, rhs.tags).append(downloads, rhs.downloads).append(clone, rhs.clone).append(commits, rhs.commits).append(self, rhs.self).append(html, rhs.html).append(hooks, rhs.hooks).isEquals();
    }

}
