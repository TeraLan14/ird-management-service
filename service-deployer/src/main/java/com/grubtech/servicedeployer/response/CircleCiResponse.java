package com.grubtech.servicedeployer.response;

import lombok.Data;

@Data
public class CircleCiResponse {
    private String number;
    private String state;
    private String id;
    private String created_at;
}
