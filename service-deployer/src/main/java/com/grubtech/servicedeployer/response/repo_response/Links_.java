
package com.grubtech.servicedeployer.response.repo_response;

import java.io.Serializable;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Links_ implements Serializable
{

    private Self_ self;
    private Html_ html;
    private Avatar_ avatar;
    private final static long serialVersionUID = 3973410587026269096L;

    public Self_ getSelf() {
        return self;
    }

    public void setSelf(Self_ self) {
        this.self = self;
    }

    public Html_ getHtml() {
        return html;
    }

    public void setHtml(Html_ html) {
        this.html = html;
    }

    public Avatar_ getAvatar() {
        return avatar;
    }

    public void setAvatar(Avatar_ avatar) {
        this.avatar = avatar;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("self", self).append("html", html).append("avatar", avatar).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(self).append(html).append(avatar).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Links_) == false) {
            return false;
        }
        Links_ rhs = ((Links_) other);
        return new EqualsBuilder().append(self, rhs.self).append(html, rhs.html).append(avatar, rhs.avatar).isEquals();
    }

}
