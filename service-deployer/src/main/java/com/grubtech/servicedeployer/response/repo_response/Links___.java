
package com.grubtech.servicedeployer.response.repo_response;

import java.io.Serializable;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Links___ implements Serializable
{

    private Self___ self;
    private Html___ html;
    private Avatar___ avatar;
    private final static long serialVersionUID = 398255479102408817L;

    public Self___ getSelf() {
        return self;
    }

    public void setSelf(Self___ self) {
        this.self = self;
    }

    public Html___ getHtml() {
        return html;
    }

    public void setHtml(Html___ html) {
        this.html = html;
    }

    public Avatar___ getAvatar() {
        return avatar;
    }

    public void setAvatar(Avatar___ avatar) {
        this.avatar = avatar;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("self", self).append("html", html).append("avatar", avatar).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(self).append(html).append(avatar).toHashCode();
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Links___) == false) {
            return false;
        }
        Links___ rhs = ((Links___) other);
        return new EqualsBuilder().append(self, rhs.self).append(html, rhs.html).append(avatar, rhs.avatar).isEquals();
    }

}
