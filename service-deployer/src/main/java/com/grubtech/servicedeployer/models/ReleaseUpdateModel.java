package com.grubtech.servicedeployer.models;

import lombok.Data;

@Data
public class ReleaseUpdateModel {
    private String id;
    private String projectSlug;
    private boolean isReleased;
}
