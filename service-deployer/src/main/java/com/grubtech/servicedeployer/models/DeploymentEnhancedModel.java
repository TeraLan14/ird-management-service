package com.grubtech.servicedeployer.models;

import lombok.Data;

@Data
public class DeploymentEnhancedModel {
    private String id;
    private String componentId;
    private String environmentId;
    private String componentName;
    private String environmentName;
    private String repositoryUrl;
    private String deployedVersionNumber;
    private String latestVersionNumber;
    private String userId;
    private boolean isDeployed;
    private String createdAt;
    private boolean delete;
    private boolean active;
}
