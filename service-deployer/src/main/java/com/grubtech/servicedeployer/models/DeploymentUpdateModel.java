package com.grubtech.servicedeployer.models;

import lombok.Data;

@Data
public class DeploymentUpdateModel {
    private String id;
    private String projectSlug;
    private boolean isDeployed;
}
