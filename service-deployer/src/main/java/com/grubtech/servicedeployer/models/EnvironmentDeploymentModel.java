package com.grubtech.servicedeployer.models;

import lombok.Data;

import java.util.List;

@Data
public class EnvironmentDeploymentModel {
    private String id;
    private String environmentName;
    private List<DeploymentEnhancedModel> deploymentEnhancedModels;
}
