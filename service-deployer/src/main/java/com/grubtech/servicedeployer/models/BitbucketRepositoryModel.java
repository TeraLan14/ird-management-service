package com.grubtech.servicedeployer.models;

import lombok.Data;

@Data
public class BitbucketRepositoryModel {
    private String slug;
    private String url;
}
