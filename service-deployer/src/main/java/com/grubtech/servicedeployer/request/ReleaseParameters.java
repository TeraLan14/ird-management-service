package com.grubtech.servicedeployer.request;

import lombok.Data;

@Data
public class ReleaseParameters {
    private boolean release;
}
