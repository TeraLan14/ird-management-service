package com.grubtech.servicedeployer.request;

import lombok.Data;

@Data
public class Parameters {

    private boolean deploy;
}
