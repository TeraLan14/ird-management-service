package com.grubtech.servicedeployer.request;

import lombok.Data;

import java.util.Map;

@Data
public class CircleciDeploymentRequest {
    private String branch;
    private Map<String, String> parameters;
}
