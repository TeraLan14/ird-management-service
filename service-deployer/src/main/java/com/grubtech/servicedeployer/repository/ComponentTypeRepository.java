package com.grubtech.servicedeployer.repository;

import com.grubtech.servicedeployer.entity.ComponentType;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@RepositoryRestResource(collectionResourceRel = "component_type", path = "component_type")
public interface ComponentTypeRepository extends MongoRepository<ComponentType, String> {
}
