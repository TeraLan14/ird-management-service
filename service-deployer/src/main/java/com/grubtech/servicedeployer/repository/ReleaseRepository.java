package com.grubtech.servicedeployer.repository;

import com.grubtech.servicedeployer.entity.Release;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@CrossOrigin
@RepositoryRestResource(collectionResourceRel = "release", path = "release")
public interface ReleaseRepository extends MongoRepository<Release, String> {

    List<Release> findByComponentId(String componentId);

    List<Release> findByComponentIdOrderByCreatedAtDesc(String componentId);

}
