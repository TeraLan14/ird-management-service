package com.grubtech.servicedeployer.repository;

import com.grubtech.servicedeployer.entity.Component;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@CrossOrigin
@RepositoryRestResource(collectionResourceRel = "component", path = "component")
public interface ComponentRepository extends MongoRepository<Component, String> {

}
