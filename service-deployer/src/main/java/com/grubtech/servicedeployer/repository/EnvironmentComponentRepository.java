package com.grubtech.servicedeployer.repository;

import com.grubtech.servicedeployer.entity.EnvironmentComponent;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;


@CrossOrigin
@RepositoryRestResource(collectionResourceRel = "environment_component", path = "environment_component")
public interface EnvironmentComponentRepository extends MongoRepository<EnvironmentComponent, String> {

    EnvironmentComponent findByEnvironmentIdAndComponentId(String environmentId,String componentId);

}
