package com.grubtech.servicedeployer.repository;

import com.grubtech.servicedeployer.entity.Environment;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@RepositoryRestResource(collectionResourceRel = "environment", path = "environment")
public interface EnvironmentRepository extends MongoRepository<Environment, String> {
}
