package com.grubtech.servicedeployer.repository;

import com.grubtech.servicedeployer.entity.Deployment;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@CrossOrigin
@RepositoryRestResource(collectionResourceRel = "deployment", path = "deployment")
public interface DeploymentRepository extends MongoRepository<Deployment, String> {

    List<Deployment> findByEnvironmentId(String environmentId);

}
