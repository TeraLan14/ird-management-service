import {Injectable} from '@angular/core';

@Injectable()
export class ComponentTypeModel {
    id: string;
    type: string;
}
