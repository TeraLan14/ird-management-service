import {Injectable} from '@angular/core';

@Injectable()
export class ReleaseUpdateModel {
    id: string;
    projectSlug: string;
    isReleased: boolean;
}
