import {Injectable} from '@angular/core';

@Injectable()
export class BitbucketRepositoryModel {
    slug: string;
    url: string;
}
