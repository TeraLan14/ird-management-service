import {Injectable} from '@angular/core';

@Injectable()
export class DeploymentUpdateModel {
    id: string;
    isDeployed: boolean;
    projectSlug: string;
}
