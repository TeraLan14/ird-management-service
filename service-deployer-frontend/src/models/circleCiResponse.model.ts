import {Injectable} from '@angular/core';

@Injectable()
export  class CircleCiResponseModel {
    number: string;
    state: string;
    id: string;
    created_at: string;
}
