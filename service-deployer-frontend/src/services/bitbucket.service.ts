import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {HttpService} from '../utils/http-service';
import {catchError, map} from 'rxjs/operators';
import {BitbucketRepositoryModel} from '../models/bitbucketRepository.model';

@Injectable()
export class BitbucketService {
    constructor(private http: HttpClient) {
    }

    getBitBucketRepositories(): Observable<BitbucketRepositoryModel[]> {
        return this.http
            .get(HttpService.SERVICE_PATH + 'bitBucket/repository/getAllRepositories', {headers: null})
            .pipe(map((response) => response as BitbucketRepositoryModel[]), catchError(this.handleError));
    }

    handleError(err) {
        if (err instanceof HttpErrorResponse) {
            return throwError(err.message)
        } else {
            return throwError(err);
        }
    }
}
